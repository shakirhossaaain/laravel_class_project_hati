@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Expense List
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <tr>
                            <th>Serial No.</th>
                            <th>Category Name</th>
                            <th>Expense Reason</th>
                            <th>Expense Amount</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        @php
                            $no = 1;
                        @endphp
                        @foreach($allExpenses as $expense)
                        <tr>
                            <td> {{ $no ++ }} </td>
                            <td> {{ $expense -> give_me_info_of_category -> category_name }} </td>
                            <td> {{ title_case($expense -> expense_reason) }} </td>
                            <td> {{ $expense -> expense_amount }} </td>
                            <td> {{ $expense -> created_at ->diffForHumans() }} </td>
                            <td>
                                @if ($expense->updated_at)
                                {{ $expense -> updated_at ->diffForHumans() }}
                                @else
                                --
                                @endif
                            </td>
                            <td>
                                <a href="{{ url('expense/edit').'/'.$expense->id }}">Edit</a>
                                |
                                <a href="{{ url('expense/delete').'/'.$expense->id }}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    {{ $allExpenses -> links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
