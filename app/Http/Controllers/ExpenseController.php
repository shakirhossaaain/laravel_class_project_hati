<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use Carbon\Carbon;
use App\Category;


class ExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function expenseadd()
    {
      $categories = Category::all();
        return view('expense.add_expence',compact('categories'));
    }

    public function expenseinsert(Request $request)
    {
        $request->validate([
            'expense_reason' => 'required',
            'expense_amount' => 'required|numeric|max:2000'
        ]);
        Expense::insert([
            'category_id' => $request->category_id,
            'expense_reason' => $request->expense_reason,
            'expense_amount' => $request->expense_amount,
            'created_at' => Carbon::now()
        ]);

        return back()->with('status','Expense Added Successfully .');
    }


    public function expenselist()
    {
        $allExpenses = Expense::paginate(5);
        return view('expense.list',compact('allExpenses'));
    }

    public function expenseedit($expense_id)
    {
        $individual_expense = Expense::select('id','expense_reason','expense_amount') -> findorfail($expense_id);
        return view('expense/update' , compact('individual_expense'));
    }

    public function expenseupdate(Request $request)
    {
        $request->validate([
            'expense_reason' => 'required',
            'expense_amount' => 'required|numeric|max:2000'
        ]);

        Expense::findorfail($request->expense_id)->update([
            'expense_reason' => $request->expense_reason,
            'expense_amount' => $request->expense_amount
        ]);

        return redirect('expense/list/view');
    }

    public function expensedelete($expense_id)
    {
        Expense::findorfail($expense_id)->delete();

        return redirect('expense/list/view');
    }

    public function expenselimit()
    {
        return view('expense/expenselimit');
    }


}
