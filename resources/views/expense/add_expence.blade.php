@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card-header">
                    Add Expense
                </div>

                <div class="card-body">
                    <form class="form" action=" {{ url('/expense/insert') }}" method="post">
                        @csrf
                        <fieldset class="form-group">
                            <label for="">Expense Category</label>
                            <select class="form-control" name="category_id">
                              <option value="">--Select One--</option>
                              @foreach($categories as $category)
                                <option value="{{ $category->id }}"> {{ $category->category_name }} </option>
                              @endforeach()
                            </select>
                            <label for="">Expense Reason</label>
                            <input class="form-control{{ $errors->has('expense_reason') ? ' is-invalid' : '' }}" type="text" name="expense_reason" value="{{old('expense_reason')}}">
                            @if ($errors->has('expense_reason'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('expense_reason') }}</strong>
                                </span>
                            @endif
                        </fieldset>

                        <fieldset class="form-group">
                            <label for="">Expense Amount</label>
                            <input class="form-control{{ $errors->has('expense_amount') ? ' is-invalid' : '' }}" type="number" name="expense_amount" value="{{old('expense_amount')}}">
                            @if ($errors->has('expense_amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('expense_amount') }}</strong>
                                </span>
                            @endif
                        </fieldset>

                        <fieldset class="text-center">
                            <button class="btn btn-success" type="submit" >SUBMIT</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
